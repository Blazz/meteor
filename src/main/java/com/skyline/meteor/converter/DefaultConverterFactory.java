package com.skyline.meteor.converter;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.skyline.common.reflection.GenericTypeUtils;
import com.skyline.common.reflection.IntrospectorUtils;
import com.skyline.common.utils.Assert;
import com.skyline.meteor.annotation.Pattern;
import com.skyline.meteor.multipart.MultipartFile;
import com.skyline.meteor.multipart.MultipartHttpServletRequest;
import com.skyline.meteor.utils.ConvertUtils;

/**
 * 默认Converter生成器
 * 
 * @author wuqh
 * 
 */
public class DefaultConverterFactory implements ConverterFactory {
	private static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";

	public Converter getUriVariableConverter(String pattern) {
		return new UriVariableConverter(pattern);
	}

	public Converter getConverter(Method method, String[] paramNames, int paramIndex, Class<?> clazz, String pattern,
			boolean isComponent) {
		Assert.notNull(clazz, "Class必须存在");

		String paramName = "";
		if (paramIndex != -1) { // BeanConverter解析每个属性的Converter时不需要paramIndex，所以传-1
			paramName = paramNames[paramIndex];
		}

		if (clazz.isArray()) {
			clazz = clazz.getComponentType();
			return createArrayConverter(method, paramNames, paramIndex, clazz, pattern, isComponent, paramName);
		}

		Converter simpleConverter = createSimpleConverter(clazz, pattern);
		if(simpleConverter != null) {
			return simpleConverter;
		}
		
		if (Collection.class.isAssignableFrom(clazz)) {
			return createCollectionConverter(method, paramNames, paramIndex, clazz, pattern, isComponent, paramName);
		}

		checkPackageName(method, clazz, paramName);

		return getBeanConverter(method, paramNames, paramIndex, clazz, pattern);

	}

	private Converter createCollectionConverter(Method method, String[] paramNames, int paramIndex, Class<?> clazz,
			String pattern, boolean isComponent, String paramName) {
		if (isComponent) {
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName + "]类型不支持，数组或集合的组成类型不能是集合");
		}

		Class<?> genericType = GenericTypeUtils.getParameterGenericType(method, paramIndex);
		if (genericType == null) {
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName + "]类型不支持，Collection实现类必须指定泛型类型");
		}
		
		Converter innerConverter = getConverter(method, paramNames, paramIndex, genericType, pattern, true);
		CollectionConverter collectionConverter = new CollectionConverter(innerConverter, genericType);

		Converter setConverter = createSetConverter(method, clazz, paramName, collectionConverter);
		if(setConverter != null) {
			return setConverter;
		}
		
		Converter listConverter = createListConverter(method, clazz, paramName, collectionConverter);
		if(listConverter != null) {
			return listConverter;
		}

		throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName
				+ "]类型不支持，请使用[java.util.List]或者[java.util.Set]");
	}

	private Converter createListConverter(Method method, Class<?> clazz, String paramName,
			CollectionConverter collectionConverter) {
		if (List.class.isAssignableFrom(clazz)) {
			if (clazz.equals(List.class) || clazz.equals(ArrayList.class)) {
				return collectionConverter;
			}
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName
					+ "]类型不支持，请使用[java.util.List]或者[java.util.ArrayList]");
		}
		
		return null;
	}

	private Converter createSetConverter(Method method, Class<?> clazz, String paramName,
			CollectionConverter collectionConverter) {
		if (SortedSet.class.isAssignableFrom(clazz)) {
			if (clazz.equals(SortedSet.class) || clazz.equals(TreeSet.class)) {
				return collectionConverter;
			}
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName
					+ "]类型不支持，请使用[java.util.SortedSet]或者[java.util.TreeSet]");
		}
		if (Set.class.isAssignableFrom(clazz)) {
			if (clazz.equals(Set.class) || clazz.equals(LinkedHashSet.class)) {
				return collectionConverter;
			}
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName
					+ "]类型不支持，请使用[java.util.Set]或者[java.util.LinkedHashSet]");
		}

		return null;
	}

	private Converter createArrayConverter(Method method, String[] paramNames, int paramIndex, Class<?> clazz,
			String pattern, boolean isComponent, String paramName) {
		if (isComponent) {
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName + "]类型不支持，数组或集合的组成类型不能是数组");
		}
		
		Converter innerConverter = getConverter(method, paramNames, paramIndex, clazz, pattern, true);
		return new ArrayConverter(innerConverter, clazz);
	}

	private void checkPackageName(Method method, Class<?> clazz, String paramName) {
		String className = clazz.getName();
		String firstPkgName = className.substring(0, className.indexOf("."));
		if (firstPkgName.equals("java")) {
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName + "]类型不支持，系统暂不支持java包下的类型");
		}
		if (firstPkgName.equals("javax")) {
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName + "]类型不支持，系统暂不支持javax包下的类型");
		}
	}

	private Converter createSimpleConverter(Class<?> clazz, String pattern) {
		if (Date.class.isAssignableFrom(clazz)) {
			String datePattern = StringUtils.isBlank(pattern) ? DEFAULT_DATE_PATTERN : pattern;
			return new SimpleTypeConverter(datePattern);
		}

		if (ConvertUtils.isConvertableSimpleType(clazz)) {
			return new SimpleTypeConverter("");
		}
		
		if (clazz.equals(HttpServletRequest.class) || clazz.equals(HttpServletResponse.class)
				|| clazz.equals(HttpSession.class) || clazz.equals(MultipartHttpServletRequest.class)
				|| clazz.equals(MultipartFile.class)) {
			return new SimpleTypeConverter("");
		}
		
		return null;
	}

	/**
	 * 生成BeanConverter
	 * 
	 * @param method
	 * @param paramNames
	 * @param paramIndex
	 * @param clazz
	 * @return
	 */
	private BeanConverter getBeanConverter(Method method, String[] paramNames, int paramIndex, Class<?> clazz,
			String methodPattern) {
		Assert.notNull(clazz, "Class必须存在");

		String paramName = "";
		if (paramIndex == -1) {
			paramName = paramNames[paramIndex];
		}

		Map<String, PropertyDescriptor> propertyDescriptors;
		BeanConverter beanConverter;
		try {
			propertyDescriptors = IntrospectorUtils.getPropertyDescriptors(clazz);
			beanConverter = new BeanConverter(clazz);
		} catch (IntrospectionException e) {
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName + "]类型不支持，解析失败：", e);
		}

		if (propertyDescriptors == null) {
			throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName + "]类型不支持，解析失败");
		}

		for (Entry<String, PropertyDescriptor> entry : propertyDescriptors.entrySet()) {
			String property = entry.getKey();
			PropertyDescriptor descriptor = entry.getValue();
			
			Method writeMethod = descriptor.getWriteMethod();
			Class<?> type = descriptor.getPropertyType();
			if (writeMethod == null || type == null) {
				continue;
			}
			
			Annotation[] annotations = writeMethod.getParameterAnnotations()[0];
			String writePattern = getPatternInWriteMethod(method, paramName, type, annotations);
			writePattern = writePattern == null ? methodPattern : writePattern;

			try {
				Converter converter = getConverter(writeMethod, paramNames, -1, type, writePattern, true);
				beanConverter.addConverter(property, converter);
			} catch (IllegalStateException e) {
				throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName
						+ "]类型不支持，含有无法解析的getter/setter方法：", e);
			}

		}

		return beanConverter;
	}

	private String getPatternInWriteMethod(Method method, String paramName, Class<?> type, Annotation[] annotations) {
		String writePattern = null;
		for (Annotation annotation : annotations) {
			if (annotation instanceof Pattern) {
				Pattern pattern = (Pattern) annotation;
				writePattern = pattern.value();
				if (!type.equals(Date.class)) {
					throw new IllegalStateException("方法[" + method + "]中的参数[" + paramName
							+ "]类型不支持，@Pattern暂时只支持用于java.util.Date类型参数上");
				}
			}
		}
		return writePattern;
	}
}
