package com.skyline.meteor.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.skyline.meteor.exception.ConvertFailedException;
import com.skyline.meteor.utils.ConvertUtils;

public class UriVariableConverter implements Converter {
	private final Object NOT_DATE_VALUE = new Object();
	private final SimpleDateFormat dateFormat;

	/**
	 * 构造函数
	 * 
	 * @param pattern
	 *            解析的日期格式（如yyyy-MM-dd）
	 */
	public UriVariableConverter(String pattern) {
		if (StringUtils.isNotBlank(pattern)) {
			dateFormat = new SimpleDateFormat(pattern);
		} else {
			dateFormat = null;
		}
	}

	@Override
	public Object convertValue(ContextProvider provider, String propertyName, Class<?> toType) {
		Map<String, String> uriTemplateVariables = provider.getUriTemplateVariables();

		if (uriTemplateVariables == null) {
			return null;
		}

		String value = uriTemplateVariables.get(propertyName);

		Object dateValue = getDateValue(toType, value);
		if(dateValue != NOT_DATE_VALUE) {
			return dateValue;
		}

		return ConvertUtils.convertValue(value, toType);
	}

	private Object getDateValue(Class<?> toType, String value) {
		if (toType.equals(Date.class) && dateFormat != null) {
			try {
				return dateFormat.parse(value);
			} catch (ParseException e) {
				throw new ConvertFailedException("解析日期格式失败", e);
			}
		}
		
		return NOT_DATE_VALUE;
	}
}
