package com.skyline.meteor.provider.internal;

import java.util.List;

import javax.servlet.ServletContext;

import com.skyline.common.asm.LocalVariableTableParameterNameDiscoverer;
import com.skyline.common.utils.ParameterNameDiscoverer;
import com.skyline.meteor.converter.ConverterFactory;
import com.skyline.meteor.converter.DefaultConverterFactory;
import com.skyline.meteor.handler.ExceptionHandler;
import com.skyline.meteor.handler.HandlerInterceptor;
import com.skyline.meteor.handler.PathDetector;
import com.skyline.meteor.handler.RequestHandler;
import com.skyline.meteor.multipart.MultipartParser;
import com.skyline.meteor.provider.commons.CommonsMultipartParser;
import com.skyline.meteor.render.JsperPageRender;
import com.skyline.meteor.render.MessageWriterFactory;
import com.skyline.meteor.render.PageRender;
import com.skyline.meteor.utils.AntPathMatcher;
import com.skyline.meteor.utils.PathMatcher;
import com.skyline.meteor.utils.UrlPathHelper;
import com.skyline.meteor.validation.ValidatorFactory;
import com.skyline.meteor.web.MeteorConfig;

/**
 * 
 * 默认配置接口实现基类
 * 
 * @author wuqh
 * 
 */
public abstract class AbstractMeteorConfig implements MeteorConfig {
	private final PathMatcher matcher = new AntPathMatcher();
	private final ParameterNameDiscoverer discoverer = new LocalVariableTableParameterNameDiscoverer();
	private final UrlPathHelper pathHelper = new UrlPathHelper();
	private final MessageWriterFactory messageWriterFactory = new InternalMessageWriterFactory();

	public abstract List<?> getControllers(ServletContext context);

	public abstract List<HandlerInterceptor> getHandlerInterceptors(ServletContext context);

	public abstract ExceptionHandler getExceptionHandler(ServletContext context);

	public abstract ValidatorFactory getValidatorFactory(ServletContext context);

	public MultipartParser getMultipartParser(ServletContext context) {
		return new CommonsMultipartParser();
	}

	public PathDetector getPathDetector(ServletContext context) {
		PathDetector detector = new PathDetector(getPathMatcher(), getUrlPathHelper(), getParameterNameDiscoverer(),
				getConverterFactory());
		detector.setHandlerInterceptors(getHandlerInterceptors(context));
		return detector;
	}

	public RequestHandler getRequestHandler(ServletContext context) {
		return new RequestHandler(getValidatorFactory(context), getMessageWriterFactory(context));
	}

	public PathMatcher getPathMatcher() {
		return matcher;
	}

	public ParameterNameDiscoverer getParameterNameDiscoverer() {
		return discoverer;
	}

	public UrlPathHelper getUrlPathHelper() {
		return pathHelper;
	}

	public MessageWriterFactory getMessageWriterFactory(ServletContext context) {
		return messageWriterFactory;
	}

	public PageRender getPageRender(ServletContext context) {
		return new JsperPageRender();
	}

	@Override
	public ConverterFactory getConverterFactory() {
		return new DefaultConverterFactory();
	}
}
