package com.skyline.meteor.provider.internal;

import com.skyline.meteor.provider.jackson.JacksonMessageWriter;
import com.skyline.meteor.render.MessageWriter;
import com.skyline.meteor.render.MessageWriterFactory;
import com.skyline.meteor.utils.WebUtils;

/**
 * MessageWriterFactory的默认实现（使用JacksonMessageWriter）
 * 
 * @author wuqh
 * 
 */
public class InternalMessageWriterFactory implements MessageWriterFactory {
	private final JacksonMessageWriter messageWriter = new JacksonMessageWriter();

	public InternalMessageWriterFactory() {
		messageWriter.setMime(WebUtils.DEFAULT_MIME);
	}

	@Override
	public MessageWriter getMessageWriterByMime(String mime) {
		return messageWriter;
	}

}
